
   <style>
    .active{
        display: inline-block;
    }
    .active-label{display: inline-block;}
    .active-label:nth-child(1){margin-right: 10px;}
    .active{margin-left: 5px;}
</style>
<div class="users form">
<?= $this->Flash->render() ?>
<?= $this->Form->create($user, ['url' => ['action' => "save/$user->id"]]) ?>
    
    <fieldset>
        <legend><?= __('Please enter your username and password') ?></legend>
        <?= $this->Form->control('firstname') ?>
        <?= $this->Form->control('lastname') ?>
        <?= $this->Form->control('email') ?>
         <div>
         
        <?= $this->Form->input('active', array(
            
                    'type'      =>  'radio',
                    'class'     =>  'active',
                    'legend'    =>  false,
                    'options'   =>  ['1'=>'Yes','0'=>'No'],
                    'value'     =>  $user->active,
                    'label' => [
                      
                        'class' => 'active-label',
                    ]
                )) ?>
            </div>
    </fieldset>
<?= $this->Form->button(__('Save')); ?>
<?= $this->Form->end() ?>
</div>
