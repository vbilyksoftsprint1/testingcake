
<?php use Cake\Routing\Router;
?>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<style>
    .greencircle{
        width: 25px;
        height: 25px;
        background: green;
        border-radius: 50%;
    }
    .graycircle{
        width: 25px;
        height: 25px;
        background: gray;
        border-radius: 50%;
    }
    
    .modal {

right:auto important;

}
    
</style>




 
<div class="container">
    <div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="modalLabel">Delete User</h4>
    </div>
    <div class="modal-body">
     Delete this user?
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:lightgray !important;    margin-top: unset !important;" >Close</button>
          <?php
    echo $this->Form->postLink(
         'Delete',
            array('action' => 'delete'),
            array('class' => 'btn btn-danger'),
            false,
         );
        ?>
    </div>
    </div>
    </div>
</div>

    <div class="row col-md-6 col-md-offset-2 custyle" style="margin-bottom:10px" > <button class="btn btn-primary "onclick="window.location.href='/users/add'">Add user</button></div>  
    <div class="row col-md-6 col-md-offset-2 custyle">
        
    <table class="table table-striped custab">
       
    <thead>
   
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Active</th>        
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <?php
foreach ($users as $user)
{
  ?>
            <tr>
                <td><?= $user->firstname?></td>
                <td><?= $user->lastname?></td>
                <td><?= $user->email?></td>
                <td><?php 
                if($user->active==1)
                { ?>
                    <div class="greencircle"></div>
                    <?php
                } else {
                ?>  <div class="graycircle"></div>
              <?php } ?>
                        </td>
                <td class="text-center">
                     <?php 
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-pencil')), 
   Router::url(
          array('action'=>'edit',$user->id)
       ), 
    array(
       'class'=>'btn btn-primary btn-confirm',

       'escape' => false), 
false);
?>  
 <?php 
echo $this->Html->link(
    $this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')), 
    '#', 
    array(
       'class'=>'btn btn-danger btn-edit',
       'data-toggle'=> 'modal',
       'data-target' => '#ConfirmDelete',
       'data-action'=> Router::url(
          array('action'=>'delete',$user->id)
       ),
       'escape' => false), 
false);
?>
                  
                   
                  
            </tr>
       <?php
}
?>
    </table>
    </div>
</div>

<script>
    $('#ConfirmDelete').on('show.bs.modal', function(e) {
    $(this).find('form').attr('action', $(e.relatedTarget).data('action'));
});
</script>