

<div class="users form">
<?= $this->Flash->render() ?>
<?= $this->Form->create(null,['url' => ['action' => "confirmPassword"]]) ?>
    
    <fieldset>
        <legend><?= __('Please enter passwords') ?></legend>
        <?= $this->Form->control('password') ?>
        <?= $this->Form->control('confirmpassword') ?>
        <input name="email" type="hidden" value="<?=$email?>" />
    </fieldset>
<?= $this->Form->button(__('Change password')); ?>
<?= $this->Form->end() ?>
</div>
