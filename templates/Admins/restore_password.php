
<div class="users form">
<?= $this->Flash->render() ?>
<?= $this->Form->create( null,['url' => ['action' => "sendEmail"]]) ?>
    
    <fieldset>
        <legend><?= __('Please enter your email') ?></legend>
        <?= $this->Form->control('email') ?>

    </fieldset>
<?= $this->Form->button(__('Send reset link')); ?>
<?= $this->Form->end() ?>
</div>
