<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('firstname', 'A lastname is required')
            ->notEmpty('lastname', 'A lastname is required')
            ->notEmpty('email', 'A email is required')
                ->notEmpty('password', 'A password is required')
            ;
    }

}
