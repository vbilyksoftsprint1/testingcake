<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\EventInterface;

class UsersController extends AppController
{

    
    public function initialize(): void
    {
        parent::initialize();
        
    }
    
  public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow([]);
         $this->viewBuilder()->setLayout('default');
    }
    public function index()
    {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }
    
    public function edit($id)
    {
        $user = $this->Users->get($id);
       // $user = $this->Users->patchEntity($user, $this->request->getData(),['validate'=>true]);
        $this->set(compact('user'));
    }

    public function add()
    {
        
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $user = $this->Users->patchEntity($user, $this->request->getData(),['validate'=>true]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }
    
     public function save($id)
    {
        $user = $this->Users->get($id);
       // dd($this->request);
        //if ($this->request->is('get')) {
            // Prior to 3.4.0 $this->request->data() was used.
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save the user.'));
        //}
        //$this->set('user', $user);
    }
    
      public function delete($id)
        {
          if($this->Users->exists($id))
          {
            $user= $this->Users->get($id);
            $result = $this->Users->delete($user);
            if($result)
            {
                $this->Flash->success(__('The user has been succesfully deleted.'));
                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error(__('Cannot delete user'));
            }
          }
          else {
              $this->Flash->error(__('User not exists'));
          }
        }
    
    
    
}

