<?php

namespace App\Controller;
use Cake\Mailer\Mailer;
use Cake\Event\EventInterface;
/**
 * Auth Controller
 *
 * @method \App\Model\Entity\Auth[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController{
 
public function initialize(): void
    {
        parent::initialize();
        
        // компонент, который делает доступ к страницам только авторизованным
//        $this->loadComponent('Auth', [
//        'Basic' => ['userModel' => 'Admins'],
//        'Form' => ['userModel' => 'Admins'],
//        'loginAction' => [
//            'controller' => 'Admins',
//            'action' => 'login',
//            'plugin' => false
//        ],
//        'authError' => 'Unauthorized Access',
//        'authenticate' => [
//            'Form' => [
//                'fields' => [
//                    'username' => 'email',
//                    'password' => 'password'
//                ],
//                'userModel'=>'Admins', //Other table than Users
//         
//            ]
//        ],
//        'loginRedirect' => [
//            'controller' => 'Users',
//            'action' => 'index'
//        ],
//        'logoutRedirect' => [
//            'controller' => 'Admins',
//            'action' => 'login'
//        ]
//    ]);
    }
    
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('default');
        $this->Auth->allow(['home', 'logout','restorePassword','sendEmail','confirmationPasswords']);
    }
        public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Неправильный логин или пароль');
        }
    }
    public function home()
    {
        $admin=[];

        $this->set(compact('admin'));
    }
    
    
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
     public function restorePassword()
    {
         
       // $this->set(compact('admin'));
    }
    
    public function sendEmail() {
        if($this->request->is('post'))
        {
             $email = $this->request->getData()['email'];    
        
        if($this->Admins->exists(['email'=>$email])){
           $mailer = new Mailer('default');
           $mailer->setFrom(['admin@mail.com' => 'My Site'])
           ->setTo($email)
           ->setSubject('Password Reset')
           ->deliver('Pls follow this link to reset password '.'<a href="localhost:8765/'. urlencode($email).'">Reset your password</a>');
            $this->Flash->success(__('Reset link sent to '. $email));
        }
        else {
            $this->Flash->error(__('Your email not found in database'));
        }
     }
     
     else {
         $this->Flash->error(__('invalid email'));
     }
    }
    
    
    public function confirmationPasswords($email) {
        $email = urldecode($email);
        $admin = $this->Admins->find()
                    ->where(['email' => $email])
                    ->first();
       if($this->Admins->exists(['email'=>$email]))
       {
           
          
       }
       else {
           $this->Flash->error(__('invalid link'));
       }
        $this->set(compact('admin'));
        $this->set(compact('email'));
    }
     public function confirmPassword() {
       // if($this->request->is('post'))
        {
            $request = $this->request->getData();
            $password = $request['password'];
            $confirmpassword = $request['confirmpassword'];
            $email = $request['email'];
            $admin = $this->Admins->find()
                    ->where(['email' => $email])
                    ->first();
            if($password == $confirmpassword && $password!='' && $confirmpassword!='')
            {
                $admin->password=$password;
                $this->Admins->save($admin);
                $this->Flash->success(__('Good passwords'));
                return $this->redirect('/users');
            }
            else {
                 $this->Flash->error(__('Passwords not equal or empty'));
                 return $this->redirect('/admins/confirmationPasswords/'.urlencode($email));
            }
        }
    }
    
}
