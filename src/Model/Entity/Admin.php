<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
/**
 * Admin Entity
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $password
 * @property string $email
 * @property int $active
 */
class Admin extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'lastname' => true,
        'password' => true,
        'email' => true,
        'active' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
    
        protected function _setPassword(string $plainPassword)
    {
        $hasher = new DefaultPasswordHasher();

        return $hasher->hash($plainPassword);
    }
    
//        public function validatePasswords($validator)
//    {
//        $validator->add('confirmpassword', 'no-misspelling', [
//            'rule' => ['compareWith', 'password'],
//            'message' => 'Passwords are not equal',
//        ]);
//
//        // ...
//
//        return $validator;
//    }
}
